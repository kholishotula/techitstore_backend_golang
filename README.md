# Dokumentasi Back End TechITStore

TechITStore adalah aplikasi e-commerce yang menyediakan berbagai macam produk berbasis teknologi.

## Data Model

<img src="data-model.png">

## Pembagian Role (spesifik)

- Admin     : mengelola kategori produk, mengubah status pembayaran

_user admin (testing):_
```
username: admin
password: admin1234
```

- Seller    : mengelola produk jualannya, melihat orderan yang masuk, mengubah status orderan pelanggan (menjadi dikemas, dikirim)

_user seller (testing):_
```
username: seller
password: seller1234
```

- Customer  : membuat orderan, mengubah status orderan (menjadi selesai atau dibatalkan)

_user customer (testing):_
```
username: customer
password: cust1234
```

## Fitur-fitur

### User

_Detail Data User_
```
name: string
username: string
email: string
phone: string
password: string
profile_photo: string (berupa url, opsional)
role: string (default: customer, pilihan: (admin, seller, customer), opsional)
```

**- Pendaftaran akun**

Rute: https://techitstore-backend.herokuapp.com/register<br>
Method: POST<br>
Berfungsi untuk membuat akun dan menambahkan data user pada database<br>
Contoh pengisian body:
```
{
    "name": "amaliah",
    "username": "amaliah",
    "email": "amaliah@gmail.com",
    "phone": "01234567890",
    "password": "amaliah1234"
}
```
Maka akan secara otomatis terdaftar sebagai role customer. Atau
```
{
    "name": "seller jaya",
    "username": "sellerjaya",
    "email": "sellerjaya@gmail.com",
    "phone": "01234567890",
    "password": "sellerjaya1234",
    "role": "seller"
}
```
untuk mendefinisikan role tertentu

**- Melihat Profil**

Rute: https://techitstore-backend.herokuapp.com/profiles<br>
Method: GET<br>
Auth: Basic Auth (semua role)<br>
Berfungsi untuk mendapatkan data profil user yang sudah login

**- Mengubah data profil**

Rute: https://techitstore-backend.herokuapp.com/profiles/update<br>
Method: PUT<br>
Auth: Basic Auth (semua role)<br>
Berfungsi untuk mengubah data profil user yang sudah login<br>
Contoh pengisian body:
```
{
    "name": "maya amaliah",
    "username": "maya",
    "email": "maya@gmail.com",
    "phone": "01234567890",
    "password": "maya1234"
}
```

<hr>

### Address

_Detail Data Address_
```
name: string
address: string
phone: string
city: string
province: string
post_code: string
primary: bool (true/false)
```

**- Menambah alamat**

Rute: https://techitstore-backend.herokuapp.com/address/add<br>
Method: POST<br>
Auth: Basic Auth (semua role)<br>
Berfungsi untuk menambah alamat pengiriman<br>
Contoh pengisian body:
```
{
    "name": "maya",
    "address": "Jl Utara Barat Daya",
    "phone": "0921864754",
    "city": "Gresik",
    "province": "Jawa Timur",
    "post_code": "12345",
    "primary": false
}
```

**- Mendapatkan daftar alamat**

Rute: https://techitstore-backend.herokuapp.com/address<br>
Method: GET<br>
Auth: Basic Auth (semua role)<br>
Berfungsi untuk mendapatkan list alamat dari user yang sudah login<br>

**- Mengubah alamat**

Rute: https://techitstore-backend.herokuapp.com/address/{address_id}/update<br>
Method: PUT<br>
Auth: Basic Auth (semua role)<br>
Berfungsi untuk mengupdate alamat pengiriman berdasarkan id<br>
Contoh pengisian body:
```
{
    "name": "maya",
    "address": "Jl Utara Timur Laut",
    "phone": "0921864754",
    "city": "Gresik",
    "province": "Jawa Timur",
    "post_code": "12345",
    "primary": false
}
```

**- Mengatur alamat pengiriman utama**

Rute: https://techitstore-backend.herokuapp.com/address/{address_id}/set-primary<br>
Method: PUT<br>
Auth: Basic Auth (semua role)<br>
Berfungsi untuk mengatur alamat pengiriman utama berdasarkan id<br>

**- Menghapus alamat**

Rute: https://techitstore-backend.herokuapp.com/address/{address_id}/delete<br>
Method: DELETE<br>
Auth: Basic Auth (semua role)<br>
Berfungsi untuk menghapus alamat pengiriman utama berdasarkan id<br>

<hr>

### Category

_Detail Data Category_
```
name: string
desc: string
```

**- Menambah kategori**

Rute: https://techitstore-backend.herokuapp.com/category/add<br>
Method: POST<br>
Auth: Basic Auth (role admin)<br>
Berfungsi untuk menambah kategori produk baru<br>
Contoh pengisian body:
```
{
    "name": "tablet",
    "desc": "Tablet adalah bentuk portabel dari notebook. Biasanya berdimensi 6-11 inch"
}
```

**- Mendapatkan daftar kategori produk**

Rute: https://techitstore-backend.herokuapp.com/category<br>
Method: GET<br>
Berfungsi untuk mendapatkan seluruh kategori produk<br>

**- Mendapatkan detail kategori produk**

Rute: https://techitstore-backend.herokuapp.com/category/{category_id}<br>
Method: GET<br>
Berfungsi untuk mendapatkan detail kategori produk<br>

**- Mengubah kategori**

Rute: https://techitstore-backend.herokuapp.com/category/{category_id}/update<br>
Method: PUT<br>
Auth: Basic Auth (role admin)<br>
Berfungsi untuk mengupdate kategori berdasarkan id<br>
Contoh pengisian body:
```
{
    "name": "tablet",
    "desc": "Tablet adalah bentuk portabel dari notebook maupun laptop. Biasanya berdimensi 6-11 inch"
}
```

<hr>

### Product

_Detail Data Product_
```
id_category: int
name: string
price: int
stock: int
desc: string
status: string (default: available, pilihan: (available, archived), opsional)
picture: string (berupa url, opsional)
```

**- Menambah produk**

Rute: https://techitstore-backend.herokuapp.com/product/add<br>
Method: POST<br>
Auth: Basic Auth (role seller)<br>
Berfungsi untuk menambah produk baru<br>
Contoh pengisian body:
```
{
    "id_category": 1,
    "name": "Asus X550",
    "price": 12000000,
    "stock": 54,
    "desc": "Laptop dengan performa mantap"
}
```

**- Mendapatkan daftar produk**

Rute: https://techitstore-backend.herokuapp.com/product<br>
Method: GET<br>
Berfungsi untuk mendapatkan daftar produk yang tersedia<br>
Params:<br>
* categoryId -> mendapatkan daftar produk berdasarkan kategori
* sellerId -> mendapatkan daftar produk berdasarkan penjual
* minPrice -> mendapatkan daftar produk dengan harga terendah adalah minPrice
* maxPrice -> mendapatkan daftar produk dengan harga tertinggi adalah maxPrice

**- Mendapatkan detail produk**

Rute: https://techitstore-backend.herokuapp.com/product/{product_id}<br>
Method: GET<br>
Berfungsi untuk mendapatkan detail produk berdasarkan id<br>

**- Mengubah produk**

Rute: https://techitstore-backend.herokuapp.com/product/{product_id}/update<br>
Method: PUT<br>
Auth: Basic Auth (role seller)<br>
Berfungsi untuk mengupdate produk yang dijualnya berdasarkan id<br>
Contoh pengisian body:
```
{
    "id_category": 1,
    "name": "Asus X550",
    "price": 12000000,
    "stock": 54,
    "desc": "Laptop mirip ROG dengan performa mantap"
}
```

**- Mengubah harga produk**

Rute: https://techitstore-backend.herokuapp.com/product/{product_id}/update-price<br>
Method: PUT<br>
Auth: Basic Auth (role seller)<br>
Berfungsi untuk mengupdate harga produk yang dijualnya berdasarkan id<br>
Contoh pengisian body:
```
{
    "price": 11000000
}
```

**- Mengubah stok produk**

Rute: https://techitstore-backend.herokuapp.com/product/{product_id}/update-stock<br>
Method: PUT<br>
Auth: Basic Auth (role seller)<br>
Berfungsi untuk mengupdate stock produk yang dijualnya berdasarkan id<br>
Contoh pengisian body:
```
{
    "stock": 99
}
```

**- Mengubah status produk menjadi available**

Rute: https://techitstore-backend.herokuapp.com/product/{product_id}/set-available<br>
Method: PUT<br>
Auth: Basic Auth (role seller)<br>
Berfungsi untuk mengupdate status produk menjadi tersedia (available) berdasarkan id<br>

**- Mengubah status produk menjadi archived**

Rute: https://techitstore-backend.herokuapp.com/product/{product_id}/set-archived<br>
Method: PUT<br>
Auth: Basic Auth (role seller)<br>
Berfungsi untuk mengupdate status produk menjadi diarsipkan (archived) berdasarkan id<br>

**- Menghapus produk**

Rute: https://techitstore-backend.herokuapp.com/product/{product_id}/delete<br>
Method: DELETE<br>
Auth: Basic Auth (role seller)<br>
Berfungsi untuk menghapus (soft delete) produk berdasarkan id<br>

<hr>

### Order, Order Item, dan Payment

_Detail Data OrderItem_
```
product_id: int
qty: int
```

_Detail Data Payment_
```
method: string (pilihan : (e-wallet, transfer bank, kartu kredit, alfamart, indomaret))
```

_Detail Data Order_
```
payment: models.Payment
order_items: []models.OrderItem
```

**- Membuat pesanan**

Rute: https://techitstore-backend.herokuapp.com/order<br>
Method: POST<br>
Auth: Basic Auth (selain role admin)<br>
Berfungsi untuk membuat pesanan baru<br>
Contoh pengisian body:
```
{
    "order_items": [
        {
            "product_id": 1,
            "qty": 2
        },
        {
            "product_id": 2,
            "qty": 1
        }
    ],
    "payment": {
        "method": "e-wallet"
    }
}
```

**- Mendapatkan detail pesanan**

Rute: https://techitstore-backend.herokuapp.com/order/{order_id}<br>
Method: GET<br>
Auth: Basic Auth (semua role)<br>
Berfungsi untuk mendapatkan detail pesanan<br>

**- Mengubah status pesanan menjadi selesai**

Rute: https://techitstore-backend.herokuapp.com/order/{order_id}/finish<br>
Method: PUT<br>
Auth: Basic Auth (role customer)<br>
Berfungsi untuk mengubah status pesanan customer berdasarkan id pesanan menjadi selesai. Customer hanya dapat mengubah status pesanannya menjadi selesai ketika pesanan telah dikirim oleh penjual<br>

**- Membatalkan pesanan**

Rute: https://techitstore-backend.herokuapp.com/order/{order_id}/cancel<br>
Method: PUT<br>
Auth: Basic Auth (role customer)<br>
Berfungsi untuk membatalkan pesanan customer berdasarkan id pesanan. Customer hanya dapat membatalkan pesanan jika penjual belum mengirim pesanannya<br>

**- Mendapatkan pesanan yang ditujukan untuk seller**

Rute: https://techitstore-backend.herokuapp.com/seller/orders<br>
Method: GET<br>
Auth: Basic Auth (role seller)<br>
Berfungsi untuk mendapatkan daftar seluruh pesanan yang ditujukan untuk seller yang telah login<br>

**- Menyetujui pesanan (mengubah statusnya menjadi dikemas)**

Rute: https://techitstore-backend.herokuapp.com/order/{order_id}/approve<br>
Method: PUT<br>
Auth: Basic Auth (role seller)<br>
Berfungsi untuk menyetujui pesanan dari customer dan mengubah status pesanannya menjadi dikemas. Seller hanya dapat menyetujui pesanan jika status pesanan sebelumnya adalah menunggu persetujuan dan customer sudah membayar pesanannya<br>

**- Mengubah status pesanan menjadi dikirim**

Rute: https://techitstore-backend.herokuapp.com/order/{order_id}/send<br>
Method: PUT<br>
Auth: Basic Auth (role seller)<br>
Berfungsi untuk mengubah status pesanan menjadi dikirim. Seller hanya dapat melakukan hal ini jika status pesanan sebelumnya adalah dikirim<br>

**- Mengubah metode pembayaran**

Rute: https://techitstore-backend.herokuapp.com/payment/{payment_id}/change-method<br>
Method: PUT<br>
Auth: Basic Auth (role customer)<br>
Berfungsi untuk mengubah metode pembayaran dan meng-generate token pembayaran baru<br>
Contoh pengisian body:
```
{
    "method": "alfamart"
}
```

**- Mengubah status pembayaran menjadi lunas**

Rute: https://techitstore-backend.herokuapp.com/payment/{payment_id}/paid<br>
Method: PUT<br>
Auth: Basic Auth (role admin)<br>
Berfungsi untuk mengubah status pembayaran menjadi lunas<br>
