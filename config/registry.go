package config

import "techitstore/models"

type Model struct {
	Model interface{}
}

func RegisterModels() []Model {
	return []Model{
		{Model: models.User{}},
		{Model: models.Address{}},
		{Model: models.Category{}},
		{Model: models.Product{}},
		{Model: models.Order{}},
		{Model: models.OrderItem{}},
		{Model: models.Payment{}},
	}
}
