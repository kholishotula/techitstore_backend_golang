package config

import (
	"net/http"

	"github.com/gorilla/mux"
)

func (server *Server) initializeRoutes() {
	server.Router = mux.NewRouter()
	server.setRouters()
}

func (server *Server) setRouters() {
	server.Post("/register", server.CreateUser)
	server.Get("/profiles", server.GetUser)
	server.Put("/profiles/update", server.UpdateUser)

	server.Post("/address/add", server.CreateAddress)
	server.Get("/address", server.GetAllAddress)
	server.Put("/address/{id}/update", server.UpdateAddress)
	server.Put("/address/{id}/set-primary", server.SetPrimaryAddress)
	server.Delete("/address/{id}/delete", server.DeleteAddress)

	server.Post("/category/add", server.CreateCategory)
	server.Get("/category", server.GetAllCategory)
	server.Get("/category/{id}", server.GetCategory)
	server.Put("/category/{id}/update", server.UpdateCategory)

	server.Post("/product/add", server.CreateProduct)
	server.Get("/product", server.GetAllProduct)
	server.Get("/product/{id}", server.GetProduct)
	server.Put("/product/{id}/update", server.UpdateProduct)
	server.Put("/product/{id}/update-price", server.UpdatePrice)
	server.Put("/product/{id}/update-stock", server.UpdateStock)
	server.Put("/product/{id}/set-available", server.SetAvailableProduct)
	server.Put("/product/{id}/set-archived", server.SetArchivedProduct)
	server.Delete("/product/{id}/delete", server.DeleteProduct)

	// customer (and seller can buy through this app)
	server.Post("/order", server.CreateOrder)
	server.Get("/order/{id}", server.GetDetailOrder)
	server.Put("/order/{id}/finish", server.FinishOrder)
	server.Put("/order/{id}/cancel", server.CancelOrder)

	// seller
	server.Get("/seller/orders", server.GetSellerOrder)
	server.Put("/order/{id}/approve", server.ApproveOrder)
	server.Put("/order/{id}/send", server.SendOrder)

	// customer
	server.Put("/payment/{id}/change-method", server.ChangePaymentMethod)
	// admin
	server.Put("/payment/{id}/paid", server.PaymentPaid)
}

// Wrap the router for GET method
func (server *Server) Get(path string, f func(w http.ResponseWriter, r *http.Request)) {
	server.Router.HandleFunc(path, f).Methods("GET")
}

// Wrap the router for POST method
func (server *Server) Post(path string, f func(w http.ResponseWriter, r *http.Request)) {
	server.Router.HandleFunc(path, f).Methods("POST")
}

// Wrap the router for PUT method
func (server *Server) Put(path string, f func(w http.ResponseWriter, r *http.Request)) {
	server.Router.HandleFunc(path, f).Methods("PUT")
}

// Wrap the router for DELETE method
func (server *Server) Delete(path string, f func(w http.ResponseWriter, r *http.Request)) {
	server.Router.HandleFunc(path, f).Methods("DELETE")
}
