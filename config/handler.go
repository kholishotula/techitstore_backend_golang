package config

import (
	"net/http"
	"techitstore/controllers"
)

func (server *Server) CreateUser(w http.ResponseWriter, r *http.Request) {
	controllers.Register(server.DB, w, r)
}

func (server *Server) GetUser(w http.ResponseWriter, r *http.Request) {
	controllers.GetProfile(server.DB, w, r)
}

func (server *Server) UpdateUser(w http.ResponseWriter, r *http.Request) {
	controllers.UpdateProfile(server.DB, w, r)
}

func (server *Server) CreateAddress(w http.ResponseWriter, r *http.Request) {
	controllers.AddAddress(server.DB, w, r)
}

func (server *Server) GetAllAddress(w http.ResponseWriter, r *http.Request) {
	controllers.GetAllAddress(server.DB, w, r)
}

func (server *Server) UpdateAddress(w http.ResponseWriter, r *http.Request) {
	controllers.UpdateAddress(server.DB, w, r)
}

func (server *Server) SetPrimaryAddress(w http.ResponseWriter, r *http.Request) {
	controllers.SetPrimaryAddress(server.DB, w, r)
}

func (server *Server) DeleteAddress(w http.ResponseWriter, r *http.Request) {
	controllers.DeleteAddress(server.DB, w, r)
}

func (server *Server) CreateCategory(w http.ResponseWriter, r *http.Request) {
	controllers.AddCategory(server.DB, w, r)
}

func (server *Server) GetAllCategory(w http.ResponseWriter, r *http.Request) {
	controllers.GetAllCategory(server.DB, w, r)
}

func (server *Server) GetCategory(w http.ResponseWriter, r *http.Request) {
	controllers.GetCategory(server.DB, w, r)
}

func (server *Server) UpdateCategory(w http.ResponseWriter, r *http.Request) {
	controllers.UpdateCategory(server.DB, w, r)
}

func (server *Server) CreateProduct(w http.ResponseWriter, r *http.Request) {
	controllers.AddProduct(server.DB, w, r)
}

func (server *Server) GetAllProduct(w http.ResponseWriter, r *http.Request) {
	controllers.GetAllProduct(server.DB, w, r)
}

func (server *Server) GetProduct(w http.ResponseWriter, r *http.Request) {
	controllers.GetProduct(server.DB, w, r)
}

func (server *Server) UpdateProduct(w http.ResponseWriter, r *http.Request) {
	controllers.UpdateProduct(server.DB, w, r)
}

func (server *Server) UpdatePrice(w http.ResponseWriter, r *http.Request) {
	controllers.UpdatePrice(server.DB, w, r)
}

func (server *Server) UpdateStock(w http.ResponseWriter, r *http.Request) {
	controllers.UpdateStock(server.DB, w, r)
}

func (server *Server) SetAvailableProduct(w http.ResponseWriter, r *http.Request) {
	controllers.SetAvailableProduct(server.DB, w, r)
}

func (server *Server) SetArchivedProduct(w http.ResponseWriter, r *http.Request) {
	controllers.SetArchivedProduct(server.DB, w, r)
}

func (server *Server) DeleteProduct(w http.ResponseWriter, r *http.Request) {
	controllers.DeleteProduct(server.DB, w, r)
}

func (server *Server) CreateOrder(w http.ResponseWriter, r *http.Request) {
	controllers.AddOrder(server.DB, w, r)
}

func (server *Server) GetDetailOrder(w http.ResponseWriter, r *http.Request) {
	controllers.GetDetailOrder(server.DB, w, r)
}

func (server *Server) FinishOrder(w http.ResponseWriter, r *http.Request) {
	controllers.FinishOrder(server.DB, w, r)
}

func (server *Server) CancelOrder(w http.ResponseWriter, r *http.Request) {
	controllers.CancelOrder(server.DB, w, r)
}

func (server *Server) GetSellerOrder(w http.ResponseWriter, r *http.Request) {
	controllers.GetSellerOrder(server.DB, w, r)
}

func (server *Server) ApproveOrder(w http.ResponseWriter, r *http.Request) {
	controllers.ApproveOrder(server.DB, w, r)
}

func (server *Server) SendOrder(w http.ResponseWriter, r *http.Request) {
	controllers.SendOrder(server.DB, w, r)
}

func (server *Server) ChangePaymentMethod(w http.ResponseWriter, r *http.Request) {
	controllers.ChangePaymentMethod(server.DB, w, r)
}

func (server *Server) PaymentPaid(w http.ResponseWriter, r *http.Request) {
	controllers.PaymentPaid(server.DB, w, r)
}
