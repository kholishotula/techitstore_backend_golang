package controllers

import (
	"net/http"
	"techitstore/models"

	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

func VerifyAuth(db *gorm.DB, w http.ResponseWriter, r *http.Request) (*models.User, bool) {
	username, password, ok := r.BasicAuth()
	if !ok {
		return nil, false
	}

	user := models.User{}
	if err := db.Take(&user, models.User{Username: username}).Error; err != nil {
		return nil, false
	}

	var matchPassword bool
	err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(password))
	if err == nil {
		matchPassword = true
	} else {
		matchPassword = false
	}

	isValid := (username == user.Username) && matchPassword
	if !isValid {
		w.Write([]byte(`wrong username/password `))
		return nil, false
	}

	return &user, true
}
