package controllers

import (
	"encoding/json"
	"net/http"
	"strconv"
	"techitstore/models"
	"techitstore/utils"

	"github.com/gorilla/mux"
	"gorm.io/gorm"
)

func AddAddress(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}

	user, verified := VerifyAuth(db, w, r)
	if !verified {
		w.Header().Set("WWW-Authenticate", "Basic realm=Restricted")
		utils.ResponseJSON(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	}

	var address models.Address
	if err := json.NewDecoder(r.Body).Decode(&address); err != nil {
		utils.ResponseJSON(w, err, http.StatusBadRequest)
		return
	}
	address.UserID = user.ID

	if errCreate := db.Save(&address).Error; errCreate != nil {
		utils.ResponseJSON(w, errCreate, http.StatusBadRequest)
		return
	}

	res := map[string]string{
		"status": "Success",
	}

	utils.ResponseJSON(w, res, http.StatusCreated)
}

func GetAllAddress(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	user, verified := VerifyAuth(db, w, r)
	if !verified {
		w.Header().Set("WWW-Authenticate", "Basic realm=Restricted")
		utils.ResponseJSON(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	}

	var addresses []models.Address
	if errGetAll := db.Find(&addresses, models.Address{UserID: user.ID}).Error; errGetAll != nil {
		utils.ResponseJSON(w, errGetAll, http.StatusBadRequest)
		return
	}

	res := map[string]interface{}{
		"status":    "Success",
		"addresses": addresses,
	}

	utils.ResponseJSON(w, res, http.StatusOK)
}

func UpdateAddress(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}

	user, verified := VerifyAuth(db, w, r)
	if !verified {
		w.Header().Set("WWW-Authenticate", "Basic realm=Restricted")
		utils.ResponseJSON(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	}

	params := mux.Vars(r)
	addressId, _ := strconv.Atoi(params["id"])

	var address models.Address
	if err := db.First(&address, models.Address{ID: addressId}).Error; err != nil {
		utils.ResponseJSON(w, err, http.StatusBadRequest)
		return
	}

	if err := json.NewDecoder(r.Body).Decode(&address); err != nil {
		utils.ResponseJSON(w, err, http.StatusBadRequest)
		return
	}
	address.UserID = user.ID

	if errCreate := db.Save(&address).Error; errCreate != nil {
		utils.ResponseJSON(w, errCreate, http.StatusBadRequest)
		return
	}

	res := map[string]string{
		"status": "Success",
	}

	utils.ResponseJSON(w, res, http.StatusOK)
}

func SetPrimaryAddress(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}

	_, verified := VerifyAuth(db, w, r)
	if !verified {
		w.Header().Set("WWW-Authenticate", "Basic realm=Restricted")
		utils.ResponseJSON(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	}

	params := mux.Vars(r)
	addressId, _ := strconv.Atoi(params["id"])

	if errUpdate := db.Model(&models.Address{}).Where("id = ?", addressId).Update("is_primary", true).Error; errUpdate != nil {
		utils.ResponseJSON(w, errUpdate, http.StatusBadRequest)
		return
	}

	res := map[string]string{
		"status": "Success",
	}

	utils.ResponseJSON(w, res, http.StatusOK)
}

func DeleteAddress(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	_, verified := VerifyAuth(db, w, r)
	if !verified {
		w.Header().Set("WWW-Authenticate", "Basic realm=Restricted")
		utils.ResponseJSON(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	}

	params := mux.Vars(r)
	addressId, _ := strconv.Atoi(params["id"])

	var address models.Address
	if err := db.First(&address, models.Address{ID: addressId}).Error; err != nil {
		utils.ResponseJSON(w, err, http.StatusBadRequest)
		return
	}

	if err := db.Delete(&address).Error; err != nil {
		utils.ResponseJSON(w, err, http.StatusInternalServerError)
		return
	}

	res := map[string]string{
		"status": "Success",
	}

	utils.ResponseJSON(w, res, http.StatusOK)
}
