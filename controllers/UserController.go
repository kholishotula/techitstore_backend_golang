package controllers

import (
	"encoding/json"
	"net/http"
	"net/mail"
	"net/url"
	"techitstore/models"
	"techitstore/utils"

	"gorm.io/gorm"
)

var roles = [...]string{"admin", "seller", "customer"}

func Register(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}

	var user models.User
	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		utils.ResponseJSON(w, err, http.StatusBadRequest)
		return
	}

	// encrypt password
	user.EncryptPassword(user.Password)

	// jika user tidak memasukkan role, maka otomatis set sebagai customer
	if user.Role == "" {
		user.Role = "customer"
	}
	// validasi
	errorValid := validateUser(user)
	if len(errorValid) > 0 {
		utils.ResponseJSON(w, errorValid, http.StatusBadRequest)
		return
	}

	if errCreate := db.Save(&user).Error; errCreate != nil {
		utils.ResponseJSON(w, errCreate, http.StatusBadRequest)
		return
	}

	res := map[string]string{
		"status": "Success",
	}

	utils.ResponseJSON(w, res, http.StatusCreated)
}

func GetProfile(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	user, verified := VerifyAuth(db, w, r)
	if !verified {
		w.Header().Set("WWW-Authenticate", "Basic realm=Restricted")
		utils.ResponseJSON(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	}

	var addresses []models.Address
	db.Find(&addresses, models.Address{UserID: user.ID})
	db.Model(&user).Association("Address").Append(addresses)

	res := map[string]interface{}{
		"status":  "Success",
		"profile": user,
	}

	utils.ResponseJSON(w, res, http.StatusOK)
}

func UpdateProfile(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}

	userOld, verified := VerifyAuth(db, w, r)
	if !verified {
		w.Header().Set("WWW-Authenticate", "Basic realm=Restricted")
		utils.ResponseJSON(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	}

	var user models.User
	if err := json.NewDecoder(r.Body).Decode(&user); err != nil {
		utils.ResponseJSON(w, err, http.StatusBadRequest)
		return
	}

	// encrypt password
	user.EncryptPassword(user.Password)

	// jika user tidak memasukkan role, maka otomatis menggunakan role lama
	if user.Role == "" {
		user.Role = userOld.Role
	}
	user.ID = userOld.ID
	// validasi
	errorValid := validateUser(user)
	if len(errorValid) > 0 {
		utils.ResponseJSON(w, errorValid, http.StatusBadRequest)
		return
	}

	if errUpdate := db.Save(&user).Error; errUpdate != nil {
		utils.ResponseJSON(w, errUpdate, http.StatusBadRequest)
		return
	}

	res := map[string]string{
		"status": "Success",
	}

	utils.ResponseJSON(w, res, http.StatusOK)
}

func validateUser(user models.User) []string {
	var errorValid []string

	if user.ProfilePhoto != "" {
		_, errUrl := url.ParseRequestURI(user.ProfilePhoto)
		if errUrl != nil {
			errorValid = append(errorValid, errUrl.Error())
		}
	}

	if !roleExist(user.Role) {
		errorValid = append(errorValid, "Role doesn't exists. Choose between admin, seller, and customer")
	}

	_, errMail := mail.ParseAddress(user.Email)
	if errMail != nil {
		errorValid = append(errorValid, errMail.Error())
	}

	return errorValid
}

func roleExist(str string) bool {
	for _, item := range roles {
		if item == str {
			return true
		}
	}
	return false
}
