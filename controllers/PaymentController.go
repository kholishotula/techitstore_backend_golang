package controllers

import (
	"encoding/json"
	"net/http"
	"strconv"
	"techitstore/models"
	"techitstore/utils"

	"github.com/gorilla/mux"
	"gorm.io/gorm"
)

var PaymentMethod = [...]string{"e-wallet", "transfer bank", "kartu kredit", "alfamart", "indomaret"}
var PaymentStatus = [...]string{"menunggu pembayaran", "lunas"}

func ChangePaymentMethod(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}

	params := mux.Vars(r)
	paymentId, _ := strconv.Atoi(params["id"])

	var payment models.Payment
	if errGet := db.First(&payment, models.Payment{ID: paymentId}).Error; errGet != nil {
		utils.ResponseJSON(w, errGet, http.StatusBadRequest)
		return
	}

	var order models.Order
	if errGet := db.First(&order, models.Order{ID: payment.OrderID}).Error; errGet != nil {
		utils.ResponseJSON(w, errGet, http.StatusBadRequest)
		return
	}

	user, verified := VerifyAuth(db, w, r)
	if !verified || user.ID != order.CustomerID {
		w.Header().Set("WWW-Authenticate", "Basic realm=Restricted")
		utils.ResponseJSON(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	}

	if payment.Status == PaymentStatus[1] {
		utils.ResponseJSON(w, "Your payment has paid", http.StatusBadRequest)
		return
	}

	var method map[string]string
	if err := json.NewDecoder(r.Body).Decode(&method); err != nil {
		utils.ResponseJSON(w, err, http.StatusBadRequest)
		return
	}

	if !PaymentMethodValid(method["method"]) {
		utils.ResponseJSON(w, "Payment method invalid", http.StatusBadRequest)
		return
	}
	payment.SetMethod(method["method"])
	payment.Token = RandomString(16) // generate new token

	if errUpdate := db.Save(&payment).Error; errUpdate != nil {
		utils.ResponseJSON(w, errUpdate, http.StatusBadRequest)
		return
	}

	res := map[string]interface{}{
		"status":        "Success",
		"payment_token": payment.Token,
		"amount_to_pay": payment.Amount,
	}

	utils.ResponseJSON(w, res, http.StatusOK)
}

func PaymentPaid(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	paymentId, _ := strconv.Atoi(params["id"])

	user, verified := VerifyAuth(db, w, r)
	if !verified || user.Role != "admin" {
		w.Header().Set("WWW-Authenticate", "Basic realm=Restricted")
		utils.ResponseJSON(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	}

	if errUpdate := db.Model(&models.Payment{}).Where("ID = ?", paymentId).Update("status", PaymentStatus[1]).Error; errUpdate != nil {
		utils.ResponseJSON(w, errUpdate, http.StatusBadRequest)
		return
	}

	res := map[string]string{
		"status": "Success",
	}

	utils.ResponseJSON(w, res, http.StatusOK)
}

func PaymentMethodValid(str string) bool {
	for _, item := range PaymentMethod {
		if item == str {
			return true
		}
	}
	return false
}
