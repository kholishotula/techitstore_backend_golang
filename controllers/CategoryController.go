package controllers

import (
	"encoding/json"
	"net/http"
	"strconv"
	"techitstore/models"
	"techitstore/utils"

	"github.com/gorilla/mux"
	"gorm.io/gorm"
)

func AddCategory(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}

	user, verified := VerifyAuth(db, w, r)
	if !verified || user.Role != "admin" {
		w.Header().Set("WWW-Authenticate", "Basic realm=Restricted")
		utils.ResponseJSON(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	}

	var category models.Category
	if err := json.NewDecoder(r.Body).Decode(&category); err != nil {
		utils.ResponseJSON(w, err, http.StatusBadRequest)
		return
	}

	if errCreate := db.Save(&category).Error; errCreate != nil {
		utils.ResponseJSON(w, errCreate, http.StatusBadRequest)
		return
	}

	res := map[string]string{
		"status": "Success",
	}

	utils.ResponseJSON(w, res, http.StatusCreated)
}

func GetAllCategory(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	var categories []models.Category
	if errGetAll := db.Find(&categories).Error; errGetAll != nil {
		utils.ResponseJSON(w, errGetAll, http.StatusBadRequest)
		return
	}

	res := map[string]interface{}{
		"status":     "Success",
		"categories": categories,
	}

	utils.ResponseJSON(w, res, http.StatusOK)
}

func GetCategory(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	categoryId, _ := strconv.Atoi(params["id"])

	var category models.Category
	if errGet := db.First(&category, models.Category{ID: categoryId}).Error; errGet != nil {
		utils.ResponseJSON(w, errGet, http.StatusBadRequest)
		return
	}

	res := map[string]interface{}{
		"status":   "Success",
		"category": category,
	}

	utils.ResponseJSON(w, res, http.StatusOK)
}

func UpdateCategory(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}

	user, verified := VerifyAuth(db, w, r)
	if !verified || user.Role != "admin" {
		w.Header().Set("WWW-Authenticate", "Basic realm=Restricted")
		utils.ResponseJSON(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	}

	params := mux.Vars(r)
	categoryId, _ := strconv.Atoi(params["id"])

	var category models.Category
	if errGet := db.First(&category, models.Category{ID: categoryId}).Error; errGet != nil {
		utils.ResponseJSON(w, errGet, http.StatusBadRequest)
		return
	}

	if err := json.NewDecoder(r.Body).Decode(&category); err != nil {
		utils.ResponseJSON(w, err, http.StatusBadRequest)
		return
	}

	if errUpdate := db.Save(&category).Error; errUpdate != nil {
		utils.ResponseJSON(w, errUpdate, http.StatusBadRequest)
		return
	}

	res := map[string]string{
		"status": "Success",
	}

	utils.ResponseJSON(w, res, http.StatusOK)
}
