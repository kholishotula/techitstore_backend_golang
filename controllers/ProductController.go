package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"techitstore/models"
	"techitstore/utils"

	"github.com/gorilla/mux"
	"gorm.io/gorm"
)

var productStatus = [...]string{"available", "archived"}

func AddProduct(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}

	user, verified := VerifyAuth(db, w, r)
	if !verified || strings.ToLower(user.Role) != "seller" {
		w.Header().Set("WWW-Authenticate", "Basic realm=Restricted")
		utils.ResponseJSON(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	}

	var product models.Product
	if err := json.NewDecoder(r.Body).Decode(&product); err != nil {
		utils.ResponseJSON(w, err, http.StatusBadRequest)
		return
	}
	product.SellerID = user.ID
	if product.Status == "" {
		if product.Stock > 0 {
			product.Status = "available"
		} else {
			product.Status = "archived"
		}
	}

	// validasi
	errorValid := validateProduct(product)
	if len(errorValid) > 0 {
		utils.ResponseJSON(w, errorValid, http.StatusBadRequest)
		return
	}

	if errCreate := db.Save(&product).Error; errCreate != nil {
		utils.ResponseJSON(w, errCreate, http.StatusBadRequest)
		return
	}

	res := map[string]string{
		"status": "Success",
	}

	utils.ResponseJSON(w, res, http.StatusCreated)
}

func GetAllProduct(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	queryValues := r.URL.Query()

	categoryId := queryValues.Get("categoryId")
	sellerId := queryValues.Get("sellerId")
	minPrice := queryValues.Get("minPrice")
	maxPrice := queryValues.Get("maxPrice")

	var products []models.Product

	queryText := fmt.Sprintf("SELECT * FROM products WHERE status = '%v'", "available")
	if categoryId != "" {
		queryText += fmt.Sprintf(" AND category_id = %v", categoryId)
	}
	if sellerId != "" {
		queryText += fmt.Sprintf(" AND seller_id = %v", sellerId)
	}
	if minPrice != "" {
		queryText += fmt.Sprintf(" AND price > %v", minPrice)
	}
	if maxPrice != "" {
		queryText += fmt.Sprintf(" AND price < %v", maxPrice)
	}
	queryText += fmt.Sprintf("ORDER BY id")

	if err := db.Raw(queryText).Scan(&products).Error; err != nil {
		utils.ResponseJSON(w, err, http.StatusBadRequest)
		return
	}

	var seller models.User
	var category models.Category
	for i := range products {
		db.First(&seller, models.User{ID: products[i].SellerID})
		db.Model(&products[i]).Association("Seller").Append(&seller)

		db.First(&category, models.Category{ID: products[i].CategoryID})
		db.Model(&products[i]).Association("Category").Append(&category)
	}

	res := map[string]interface{}{
		"status":   "Success",
		"products": products,
	}

	utils.ResponseJSON(w, res, http.StatusOK)
}

func GetProduct(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	productId, _ := strconv.Atoi(params["id"])

	var product models.Product
	if errGet := db.First(&product, models.Product{ID: productId}).Error; errGet != nil {
		utils.ResponseJSON(w, errGet, http.StatusBadRequest)
		return
	}

	var seller models.User
	db.First(&seller, models.User{ID: product.SellerID})
	db.Model(&product).Association("Seller").Append(&seller)

	var category models.Category
	db.First(&category, models.Category{ID: product.CategoryID})
	db.Model(&product).Association("Category").Append(&category)

	res := map[string]interface{}{
		"status":  "Success",
		"product": product,
	}

	utils.ResponseJSON(w, res, http.StatusOK)
}

func UpdateProduct(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}

	params := mux.Vars(r)
	productId, _ := strconv.Atoi(params["id"])

	var product models.Product
	if errGet := db.First(&product, models.Product{ID: productId}).Error; errGet != nil {
		utils.ResponseJSON(w, errGet, http.StatusBadRequest)
		return
	}

	seller, verified := VerifyAuth(db, w, r)
	if !verified || seller.ID != product.SellerID {
		w.Header().Set("WWW-Authenticate", "Basic realm=Restricted")
		utils.ResponseJSON(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	}

	if err := json.NewDecoder(r.Body).Decode(&product); err != nil {
		utils.ResponseJSON(w, err, http.StatusBadRequest)
		return
	}
	product.SellerID = seller.ID
	if product.Status == "" {
		if product.Stock > 0 {
			product.Status = "available"
		} else {
			product.Status = "archived"
		}
	}

	// validasi
	errorValid := validateProduct(product)
	if len(errorValid) > 0 {
		utils.ResponseJSON(w, errorValid, http.StatusBadRequest)
		return
	}

	if errUpdate := db.Save(&product).Error; errUpdate != nil {
		utils.ResponseJSON(w, errUpdate, http.StatusBadRequest)
		return
	}

	res := map[string]string{
		"status": "Success",
	}

	utils.ResponseJSON(w, res, http.StatusOK)
}

func UpdatePrice(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}

	params := mux.Vars(r)
	productId, _ := strconv.Atoi(params["id"])

	var product models.Product
	if errGet := db.First(&product, models.Product{ID: productId}).Error; errGet != nil {
		utils.ResponseJSON(w, errGet, http.StatusBadRequest)
		return
	}

	user, verified := VerifyAuth(db, w, r)
	if !verified || product.SellerID != user.ID {
		w.Header().Set("WWW-Authenticate", "Basic realm=Restricted")
		utils.ResponseJSON(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	}

	var price map[string]int
	if err := json.NewDecoder(r.Body).Decode(&price); err != nil {
		utils.ResponseJSON(w, err, http.StatusBadRequest)
		return
	}
	product.SetPrice(price["price"])

	if errUpdate := db.Save(&product).Error; errUpdate != nil {
		utils.ResponseJSON(w, errUpdate, http.StatusBadRequest)
		return
	}

	res := map[string]string{
		"status": "Success",
	}

	utils.ResponseJSON(w, res, http.StatusOK)
}

func UpdateStock(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}

	params := mux.Vars(r)
	productId, _ := strconv.Atoi(params["id"])

	var product models.Product
	if errGet := db.First(&product, models.Product{ID: productId}).Error; errGet != nil {
		utils.ResponseJSON(w, errGet, http.StatusBadRequest)
		return
	}

	user, verified := VerifyAuth(db, w, r)
	if !verified || product.SellerID != user.ID {
		w.Header().Set("WWW-Authenticate", "Basic realm=Restricted")
		utils.ResponseJSON(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	}

	var stock map[string]int
	if err := json.NewDecoder(r.Body).Decode(&stock); err != nil {
		utils.ResponseJSON(w, err, http.StatusBadRequest)
		return
	}
	product.SetStock(stock["stock"])

	if errUpdate := db.Save(&product).Error; errUpdate != nil {
		utils.ResponseJSON(w, errUpdate, http.StatusBadRequest)
		return
	}

	res := map[string]string{
		"status": "Success",
	}

	utils.ResponseJSON(w, res, http.StatusOK)
}

func SetAvailableProduct(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}

	params := mux.Vars(r)
	productId, _ := strconv.Atoi(params["id"])

	var product models.Product
	if errGet := db.First(&product, models.Product{ID: productId}).Error; errGet != nil {
		utils.ResponseJSON(w, errGet, http.StatusBadRequest)
		return
	}

	user, verified := VerifyAuth(db, w, r)
	if !verified || product.SellerID != user.ID {
		w.Header().Set("WWW-Authenticate", "Basic realm=Restricted")
		utils.ResponseJSON(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	}

	product.SetAvailable()

	if errUpdate := db.Save(&product).Error; errUpdate != nil {
		utils.ResponseJSON(w, errUpdate, http.StatusBadRequest)
		return
	}

	res := map[string]string{
		"status": "Success",
	}

	utils.ResponseJSON(w, res, http.StatusOK)
}

func SetArchivedProduct(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}

	params := mux.Vars(r)
	productId, _ := strconv.Atoi(params["id"])

	var product models.Product
	if errGet := db.First(&product, models.Product{ID: productId}).Error; errGet != nil {
		utils.ResponseJSON(w, errGet, http.StatusBadRequest)
		return
	}

	user, verified := VerifyAuth(db, w, r)
	if !verified || product.SellerID != user.ID {
		w.Header().Set("WWW-Authenticate", "Basic realm=Restricted")
		utils.ResponseJSON(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	}

	product.SetArchived()

	if errUpdate := db.Save(&product).Error; errUpdate != nil {
		utils.ResponseJSON(w, errUpdate, http.StatusBadRequest)
		return
	}

	res := map[string]string{
		"status": "Success",
	}

	utils.ResponseJSON(w, res, http.StatusOK)
}

func DeleteProduct(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	productId, _ := strconv.Atoi(params["id"])

	var product models.Product
	if err := db.First(&product, models.Product{ID: productId}).Error; err != nil {
		utils.ResponseJSON(w, err, http.StatusBadRequest)
		return
	}

	user, verified := VerifyAuth(db, w, r)
	if !verified || product.SellerID != user.ID {
		w.Header().Set("WWW-Authenticate", "Basic realm=Restricted")
		utils.ResponseJSON(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	}

	if err := db.Delete(&product).Error; err != nil {
		utils.ResponseJSON(w, err, http.StatusInternalServerError)
		return
	}

	res := map[string]string{
		"status": "Success",
	}

	utils.ResponseJSON(w, res, http.StatusOK)
}

func validateProduct(product models.Product) []string {
	var errorValid []string

	if product.Picture != "" {
		_, errUrl := url.ParseRequestURI(product.Picture)
		if errUrl != nil {
			errorValid = append(errorValid, errUrl.Error())
		}
	}

	if !productStatusValid(product.Status) {
		errorValid = append(errorValid, "Status is not valid. Choose between available or archived")
	}

	return errorValid
}

func productStatusValid(str string) bool {
	for _, item := range productStatus {
		if item == str {
			return true
		}
	}
	return false
}
