package controllers

import (
	"crypto/rand"
	"encoding/hex"
	"encoding/json"
	"net/http"
	"strconv"
	"strings"
	"techitstore/models"
	"techitstore/utils"
	"time"

	"github.com/gorilla/mux"
	"gorm.io/gorm"
)

var orderStatus = [...]string{"menunggu persetujuan", "dikemas", "dikirim", "selesai", "dibatalkan"}

func AddOrder(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Content-Type") != "application/json" {
		http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
		return
	}

	user, verified := VerifyAuth(db, w, r)
	if !verified || strings.ToLower(user.Role) == "admin" {
		w.Header().Set("WWW-Authenticate", "Basic realm=Restricted")
		utils.ResponseJSON(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	}

	var order models.Order
	if err := json.NewDecoder(r.Body).Decode(&order); err != nil {
		utils.ResponseJSON(w, err, http.StatusBadRequest)
		return
	}

	order.CustomerID = user.ID
	order.Code = RandomString(20)
	order.OrderTime = time.Now()
	order.Status = orderStatus[0]

	var order_items []models.OrderItem
	var seller_id int
	for i := range order.OrderItems {
		var product models.Product
		db.First(&product, models.Product{ID: order.OrderItems[i].ProductID})
		if i == 0 {
			seller_id = product.SellerID
		} else {
			if product.SellerID != seller_id {
				utils.ResponseJSON(w, "Error. Orders must be from the same seller", http.StatusBadRequest)
				return
			}
		}
		order_items = append(order_items, models.OrderItem{
			ProductID: order.OrderItems[i].ProductID,
			Qty:       order.OrderItems[i].Qty,
			SubTotal:  (product.Price * order.OrderItems[i].Qty),
		})
	}

	order.SellerID = seller_id
	var total_price = 0
	for i := range order_items {
		total_price += order_items[i].SubTotal
	}
	order.TotalPrice = total_price
	order.OrderItems = order_items

	if !PaymentMethodValid(order.Payment.Method) {
		utils.ResponseJSON(w, "Error. Payment method invalid", http.StatusBadRequest)
		return
	}
	payment := models.Payment{
		Token:  RandomString(16),
		Amount: total_price,
		Method: order.Payment.Method,
		Status: PaymentStatus[0],
	}
	order.Payment = payment

	order.ApprovedAt = nil
	order.CancelledAt = nil

	if errCreate := db.Save(&order).Error; errCreate != nil {
		utils.ResponseJSON(w, errCreate, http.StatusBadRequest)
		return
	}

	// decrease product stock
	for i := range order.OrderItems {
		var product models.Product
		db.First(&product, models.Product{ID: order.OrderItems[i].ProductID})
		product.SetStock(product.Stock - order.OrderItems[i].Qty)
		db.Save(&product)
	}

	res := map[string]interface{}{
		"status":        "Success",
		"order_code":    order.Code,
		"payment_token": order.Payment.Token,
		"amount_to_pay": order.Payment.Amount,
	}

	utils.ResponseJSON(w, res, http.StatusCreated)
}

func GetDetailOrder(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	_, verified := VerifyAuth(db, w, r)
	if !verified {
		w.Header().Set("WWW-Authenticate", "Basic realm=Restricted")
		utils.ResponseJSON(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	}

	params := mux.Vars(r)
	orderId, _ := strconv.Atoi(params["id"])

	var order models.Order
	if errGet := db.First(&order, models.Order{ID: orderId}).Error; errGet != nil {
		utils.ResponseJSON(w, errGet, http.StatusBadRequest)
		return
	}

	var seller models.User
	db.First(&seller, models.User{ID: order.SellerID})
	db.Model(&order).Association("Seller").Append(&seller)

	var customer models.User
	db.First(&customer, models.User{ID: order.CustomerID})
	db.Model(&order).Association("Customer").Append(&customer)

	var payment models.Payment
	if err := db.First(&payment, models.Payment{OrderID: order.ID}).Error; err != nil {
		utils.ResponseJSON(w, err, http.StatusBadRequest)
		return
	}
	db.Model(&order).Association("Payment").Append(&payment)

	var order_items []models.OrderItem
	db.Find(&order_items, models.OrderItem{OrderID: order.ID})

	for i := range order_items {
		var product models.Product
		db.First(&product, models.Product{ID: order_items[i].ProductID})

		var seller models.User
		db.First(&seller, models.User{ID: product.SellerID})
		db.Model(&product).Association("Seller").Append(&seller)

		var category models.Category
		db.First(&category, models.Category{ID: product.CategoryID})
		db.Model(&product).Association("Category").Append(&category)

		db.Model(&order_items[i]).Association("Product").Append(&product)
	}

	db.Model(&order).Association("OrderItems").Append(order_items)

	res := map[string]interface{}{
		"status": "Success",
		"order":  order,
	}

	utils.ResponseJSON(w, res, http.StatusOK)
}

func FinishOrder(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	orderId, _ := strconv.Atoi(params["id"])

	var order models.Order
	if errGet := db.First(&order, models.Order{ID: orderId}).Error; errGet != nil {
		utils.ResponseJSON(w, errGet, http.StatusBadRequest)
		return
	}

	user, verified := VerifyAuth(db, w, r)
	if !verified || order.CustomerID != user.ID {
		w.Header().Set("WWW-Authenticate", "Basic realm=Restricted")
		utils.ResponseJSON(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	}

	if order.Status == orderStatus[2] {
		order.Status = orderStatus[3]
	} else {
		utils.ResponseJSON(w, "Error. Your order has not sent", http.StatusBadRequest)
		return
	}

	if errUpdate := db.Save(&order).Error; errUpdate != nil {
		utils.ResponseJSON(w, errUpdate, http.StatusBadRequest)
		return
	}

	res := map[string]string{
		"status": "Success",
	}

	utils.ResponseJSON(w, res, http.StatusOK)
}

func CancelOrder(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	orderId, _ := strconv.Atoi(params["id"])

	var order models.Order
	if errGet := db.First(&order, models.Order{ID: orderId}).Error; errGet != nil {
		utils.ResponseJSON(w, errGet, http.StatusBadRequest)
		return
	}

	user, verified := VerifyAuth(db, w, r)
	if !verified || order.CustomerID != user.ID {
		w.Header().Set("WWW-Authenticate", "Basic realm=Restricted")
		utils.ResponseJSON(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	}

	if order.Status == orderStatus[0] || order.Status == orderStatus[1] {
		order.Status = orderStatus[4]

		currentTime := time.Now()
		order.CancelledAt = &currentTime
	} else {
		utils.ResponseJSON(w, "Error. Your order has been sent. You cannot cancel it", http.StatusBadRequest)
		return
	}

	if errUpdate := db.Save(&order).Error; errUpdate != nil {
		utils.ResponseJSON(w, errUpdate, http.StatusBadRequest)
		return
	}

	res := map[string]string{
		"status": "Success",
	}

	utils.ResponseJSON(w, res, http.StatusOK)
}

func GetSellerOrder(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	user, verified := VerifyAuth(db, w, r)
	if !verified || user.Role != "seller" {
		w.Header().Set("WWW-Authenticate", "Basic realm=Restricted")
		utils.ResponseJSON(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	}

	var orders []models.Order
	if errGet := db.Find(&orders, models.Order{SellerID: user.ID}).Error; errGet != nil {
		utils.ResponseJSON(w, errGet, http.StatusBadRequest)
		return
	}

	for i := range orders {
		var seller models.User
		db.First(&seller, models.User{ID: orders[i].SellerID})
		db.Model(&orders[i]).Association("Seller").Append(&seller)

		var customer models.User
		db.First(&customer, models.User{ID: orders[i].CustomerID})
		db.Model(&orders[i]).Association("Customer").Append(&customer)

		var payment models.Payment
		if err := db.First(&payment, models.Payment{OrderID: orders[i].ID}).Error; err != nil {
			utils.ResponseJSON(w, err, http.StatusBadRequest)
			return
		}
		db.Model(&orders[i]).Association("Payment").Append(&payment)

		var order_items []models.OrderItem
		db.Find(&order_items, models.OrderItem{OrderID: orders[i].ID})

		for i := range order_items {
			var product models.Product
			db.First(&product, models.Product{ID: order_items[i].ProductID})

			var seller models.User
			db.First(&seller, models.User{ID: product.SellerID})
			db.Model(&product).Association("Seller").Append(&seller)

			var category models.Category
			db.First(&category, models.Category{ID: product.CategoryID})
			db.Model(&product).Association("Category").Append(&category)

			db.Model(&order_items[i]).Association("Product").Append(&product)
		}

		db.Model(&orders[i]).Association("OrderItems").Append(order_items)
	}

	res := map[string]interface{}{
		"status": "Success",
		"orders": orders,
	}

	utils.ResponseJSON(w, res, http.StatusOK)
}

func ApproveOrder(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	orderId, _ := strconv.Atoi(params["id"])

	var order models.Order
	if errGet := db.First(&order, models.Order{ID: orderId}).Error; errGet != nil {
		utils.ResponseJSON(w, errGet, http.StatusBadRequest)
		return
	}

	user, verified := VerifyAuth(db, w, r)
	if !verified || order.SellerID != user.ID {
		w.Header().Set("WWW-Authenticate", "Basic realm=Restricted")
		utils.ResponseJSON(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	}

	var payment models.Payment
	if errGet := db.First(&payment, models.Payment{OrderID: orderId}).Error; errGet != nil {
		utils.ResponseJSON(w, errGet, http.StatusBadRequest)
		return
	}

	if order.Status == orderStatus[0] && payment.Status == PaymentStatus[1] {
		order.Status = orderStatus[1]

		currentTime := time.Now()
		order.ApprovedAt = &currentTime
	} else if payment.Status == PaymentStatus[0] {
		utils.ResponseJSON(w, "Error. Customer has not complete the paymnet", http.StatusBadRequest)
		return
	} else {
		utils.ResponseJSON(w, "Error. The order has been sent", http.StatusBadRequest)
		return
	}

	if errUpdate := db.Save(&order).Error; errUpdate != nil {
		utils.ResponseJSON(w, errUpdate, http.StatusBadRequest)
		return
	}

	res := map[string]string{
		"status": "Success",
	}

	utils.ResponseJSON(w, res, http.StatusOK)
}

func SendOrder(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	orderId, _ := strconv.Atoi(params["id"])

	var order models.Order
	if errGet := db.First(&order, models.Order{ID: orderId}).Error; errGet != nil {
		utils.ResponseJSON(w, errGet, http.StatusBadRequest)
		return
	}

	user, verified := VerifyAuth(db, w, r)
	if !verified || order.SellerID != user.ID {
		w.Header().Set("WWW-Authenticate", "Basic realm=Restricted")
		utils.ResponseJSON(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
		return
	}

	if order.Status == orderStatus[1] {
		order.Status = orderStatus[2]
	} else if order.Status == orderStatus[0] {
		utils.ResponseJSON(w, "Error. Your order has not approved", http.StatusBadRequest)
		return
	} else {
		utils.ResponseJSON(w, "Error. Your order has been sent. You cannot cancel it", http.StatusBadRequest)
		return
	}

	if errUpdate := db.Save(&order).Error; errUpdate != nil {
		utils.ResponseJSON(w, errUpdate, http.StatusBadRequest)
		return
	}

	res := map[string]string{
		"status": "Success",
	}

	utils.ResponseJSON(w, res, http.StatusOK)
}

// function to generate order code and payment token
func RandomString(length int) string {
	token := make([]byte, length)
	if _, err := rand.Read(token); err != nil {
		return ""
	}
	return hex.EncodeToString(token)
}
