package models

import (
	"time"

	"gorm.io/gorm"
)

type Product struct {
	ID          int `gorm:"not null;uniqueIndex;primary_key"`
	Seller      User
	SellerID    int `gorm:"index"`
	Category    Category
	CategoryID  int    `gorm:"index" json:"id_category"`
	ProductName string `gorm:"size:100;not null" json:"name"`
	Price       int    `gorm:"not null" json:"price"`
	Stock       int    `gorm:"not null" json:"stock"`
	Description string `gorm:"size:255;not null" json:"desc"`
	Status      string `gorm:"size:50;not null" json:"status"`
	Picture     string `gorm:"size:255;not null" json:"picture"`
	CreatedAt   time.Time
	UpdatedAt   time.Time
	DeletedAt   gorm.DeletedAt
}

func (product *Product) SetPrice(price int) {
	product.Price = price
}

func (product *Product) SetStock(stock int) {
	product.Stock = stock
}

func (product *Product) SetAvailable() {
	product.Status = "available"
}

func (product *Product) SetArchived() {
	product.Status = "archived"
}
