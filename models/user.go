package models

import (
	"time"

	"golang.org/x/crypto/bcrypt"
)

type User struct {
	ID           int    `gorm:"not null;uniqueIndex;primary_key" json:"id"`
	Name         string `gorm:"size:100;not null" json:"name"`
	Username     string `gorm:"size:100;not null;uniqueIndex" json:"username"`
	Email        string `gorm:"size:100;not null;uniqueIndex" json:"email"`
	Phone        string `gorm:"size:13;not null" json:"phone"`
	Password     string `gorm:"size:255;not null" json:"password"`
	ProfilePhoto string `gorm:"size:255" json:"profile_photo"`
	Role         string `gorm:"size:100;not null" json:"role"`
	Address      []Address
	CreatedAt    time.Time `json:"created_at"`
	UpdatedAt    time.Time `json:"updated_at"`
}

func (user *User) EncryptPassword(password string) {
	hash, _ := bcrypt.GenerateFromPassword([]byte(password), 8)
	user.Password = string(hash)
}
