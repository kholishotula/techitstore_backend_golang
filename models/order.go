package models

import (
	"time"
)

type Order struct {
	ID          int `gorm:"not null;uniqueIndex;primary_key"`
	Seller      User
	SellerID    int `gorm:"index"`
	Customer    User
	CustomerID  int         `gorm:"index"`
	Code        string      `gorm:"size:100;not null;uniqueIndex"`
	OrderTime   time.Time   `gorm:"not null"`
	TotalPrice  int         `gorm:"not null"`
	Status      string      `gorm:"size:50;not null"`
	Payment     Payment     `json:"payment"`
	OrderItems  []OrderItem `json:"order_items"`
	ApprovedAt  *time.Time
	CancelledAt *time.Time
	CreatedAt   time.Time
	UpdatedAt   time.Time
}
