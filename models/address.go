package models

import "time"

type Address struct {
	ID            int    `gorm:"not null;uniqueIndex;primary_key"`
	UserID        int    `gorm:"index"`
	RecipientName string `gorm:"size:100" json:"name"`
	Address1      string `gorm:"size:255" json:"address"`
	Phone         string `gorm:"size:13" json:"phone"`
	City          string `gorm:"size:100" json:"city"`
	Province      string `gorm:"size:100" json:"province"`
	PostCode      string `gorm:"size:5" json:"post_code"`
	IsPrimary     bool   `json:"primary"`
	CreatedAt     time.Time
	UpdatedAt     time.Time
}
