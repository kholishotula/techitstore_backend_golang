package models

import "time"

type Category struct {
	ID           int    `gorm:"not null;uniqueIndex;primary_key"`
	CategoryName string `gorm:"size:100;not null" json:"name"`
	Description  string `gorm:"size:255;not null" json:"desc"`
	CreatedAt    time.Time
	UpdatedAt    time.Time
}
