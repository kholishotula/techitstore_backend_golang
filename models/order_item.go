package models

import "time"

type OrderItem struct {
	ID        int `gorm:"not null;uniqueIndex;primary_key"`
	OrderID   int `gorm:"index"`
	Product   Product
	ProductID int `gorm:"index" json:"product_id"`
	Qty       int `gorm:"not null" json:"qty"`
	SubTotal  int `gorm:"not null"`
	CreatedAt time.Time
	UpdatedAt time.Time
}
