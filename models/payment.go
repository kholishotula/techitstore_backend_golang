package models

import "time"

type Payment struct {
	ID        int    `gorm:"not null;uniqueIndex;primary_key"`
	OrderID   int    `gorm:"index"`
	Token     string `gorm:"size:100;not null;index"`
	Amount    int
	Method    string `gorm:"size:50;not null" json:"method"`
	Status    string `gorm:"size:50;not null"`
	CreatedAt time.Time
	UpdatedAt time.Time
}

func (payment *Payment) SetMethod(method string) {
	payment.Method = method
}
